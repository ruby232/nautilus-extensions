import os, subprocess
from gi.repository import Nautilus, GObject
from gi import require_version
require_version('Gtk', '3.0')
require_version('Nautilus', '3.0')
from gettext import gettext, bindtextdomain, textdomain
try:
	# python 8
	from gettext import locale
except ImportError:
	# python 9
	import locale
	
class OpenGuakeHere(Nautilus.MenuProvider, GObject.GObject):
	def __init__(self):
		pass
	def get_file_items(self, window, files):
		pass
	
	def get_background_items(self, window, file):
		self._setup_gettext()
		self.window = window
		item = Nautilus.MenuItem(name="OpenGuakeHere::Nautilus",
                                 label=gettext("Open Guake Here"),
                                 tip=gettext("Open in Guake terminal here."))
		item.connect("activate", self._nautilus_run, file)
		return [item,]

	def _nautilus_run(self, menu, file):
		uri = file.get_uri()
		clear_uri = uri.replace("file://", "")
		print('"%s"' % clear_uri)
		subprocess.Popen(['guake', '-n', clear_uri, '--show'])

	def _setup_gettext(self):
		try: # prevent a possible exception
			locale.setlocale(locale.LC_ALL, "")
		except:
			pass
		bindtextdomain("open-guake-here", "/usr/share/locale")
		textdomain("open-guake-here")
