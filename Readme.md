# Nautilus extensions

Extencions for GNOME Nautiles

1. Copy content in ~/.local/share/nautilus-python/extensions/
2. Restart nautilus

```bash
# exit nautiles
nautilus -q

# open nautiles
nautilus

# open debug mode
NAUTILUS_PYTHON_DEBUG=misc nautilus
```
